
/**
 * Created by Administrator1 on 2017/4/21.
 */
public class MatchGameHandler {
    private int[] matchGameBuffer;
    private int top;
    private int maxSize;
    private static MatchGameHandler mgHdler = null;
    public static MatchGameHandler getMgHdler(){   //manage the waiting queue for matching game
        if(mgHdler == null)
            mgHdler = new MatchGameHandler();
        return  mgHdler;
    }
    public MatchGameHandler(){
        matchGameBuffer = new int[1000];
        maxSize = 1000;
        top = 0;
    }
    public boolean push(int id){
        if(isFull()){
            return false;
        }else {
            matchGameBuffer[top++] = id;
            return true;
        }
    }
    public int pop(){
        if(isEmpty()){
            return -1;
        }else {
            int tmp = matchGameBuffer[0];
            for(int i = 0;i < top - 1;i++){
                matchGameBuffer[i] = matchGameBuffer[i+1];
            }
            top --;
            return tmp;
        }
    }
    private boolean isEmpty(){
        return top <= 0;
    }
    private boolean isFull(){
        return top >= maxSize;
    }
    public void print(){
        for(int i = 0 ; i< maxSize;i++){
            System.out.print(matchGameBuffer[i] +  " ");
        }
        System.out.print("\n");
    }
    public void delete(int id){
        for(int i = 0;i <= top - 1;i++){
            if(matchGameBuffer[i] == id){
                for(int j = i;j < top - 1;j++){
                    matchGameBuffer[j] = matchGameBuffer[j+1];
                }
                top--;
                break;
            }
        }
    }
}
