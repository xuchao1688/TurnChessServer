import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Scanner;

import entity.*;

/**
 * Created by Administrator1 on 2017/3/26.
 */
public class Main {                       //the main part of server
    public static void main(String args[]) {
        ChessHandler chessHandler = new ChessHandler();
        chessHandler.generateChessBoard();
        DBAccess.getDBA();                   //connect to MySQL
        try {
            ServerSocket serversocket = new ServerSocket(2333);
            final int userScale = 1000;
            ObjectOutputStream ooslist[] = new ObjectOutputStream[userScale];
            ObjectInputStream oislist[] = new ObjectInputStream[userScale];   //manage all connections
            while (true) {
                final Socket clientsocket = serversocket.accept();         //wait for client to connect
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int id = -1;
                        try {
                            OutputStream os = clientsocket.getOutputStream();
                            InputStream is = clientsocket.getInputStream();
                            ObjectOutputStream oos = new ObjectOutputStream(os);
                            ObjectInputStream ois = new ObjectInputStream(is);
                            RequestObject reqObj = null;
                            ResponseObject resObj = null;
                            while (true) {
                                reqObj = (RequestObject) ois.readObject();
                                System.out.println(reqObj);
                                switch (reqObj.getReqType()) {              //deal with different request types
                                    case RequestObject.REQ_REG:
                                        RegInfo regInfo = (RegInfo) reqObj.getReqBody();
                                        ResultSet rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM users WHERE BINARY account='" +
                                                regInfo.getAccount() + "'LIMIT 1;");
                                        rs.next();
                                        int count = rs.getInt("COUNT(*)");
                                        if (count == 0) {
                                            rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM users;");
                                            rs.next();
                                            id = rs.getInt("COUNT(*)");
                                            DBAccess.getDBA().execute("INSERT INTO users(uid,account,password,online,in_battle,win,lose) " +
                                                    "VALUES(" + id + ",'" + regInfo.getAccount() + "','" + regInfo.getPassword() + "',0,0,0,0" + ");");
                                            DBAccess.getDBA().executeUpdate("CREATE TABLE contact_" + id + "(" +
                                                    "uid INT(11) NOT NULL PRIMARY KEY," +
                                                    "account VARCHAR(20) BINARY NOT NULL);");
                                            DBAccess.getDBA().executeUpdate("CREATE TABLE newfriend_" + id + "(" +
                                                    "uid INT(11) NOT NULL PRIMARY KEY," +
                                                    "account VARCHAR(20) BINARY NOT NULL);");
//                                            DBAccess.getDBA().execute("INSERT INTO contact_"+id+"(uid,account,online) " +
//                                                    "VALUES(" + 21 + ",'" + "luwenjie" + "',"  + 0 + ");");
                                            resObj = new ResponseObject(ResponseObject.RES_REG, new RegInfoRes("successfully register", true));
                                        } else {
                                            resObj = new ResponseObject(ResponseObject.RES_REG, new RegInfoRes("fail to register", false));
                                        }
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_LOG:
                                        LogInfo logInfo = (LogInfo) reqObj.getReqBody();
                                        rs = DBAccess.getDBA().executeQuery("SELECT uid FROM users WHERE BINARY password='" +
                                                logInfo.getPassword() + "' AND BINARY account='" +
                                                logInfo.getAccount() + "'LIMIT 1;");
                                        if(rs.next()){
                                            id = rs.getInt("uid");
                                            DBAccess.getDBA().execute("UPDATE users SET online=1 WHERE uid="
                                                    +id+";");
                                            resObj = new ResponseObject(ResponseObject.RES_LOG, new LogInfoRes("successfully login in",true));
                                            ooslist[id] = oos;
                                            oislist[id] = ois;
                                        }
                                        else{
                                            resObj = new ResponseObject(ResponseObject.RES_LOG, new LogInfoRes("fail to login in",false));
                                        }
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_ADDNEWFRIEND:
                                        NewFriendInfo newFriendInfo = (NewFriendInfo)reqObj.getReqBody();
                                        rs = DBAccess.getDBA().executeQuery("SELECT uid FROM users WHERE BINARY account='"
                                                + newFriendInfo.getUsername() + "' LIMIT 1;");
                                        id = findSocketID(ooslist,oos);
                                        if(rs.next()){
                                            ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT account FROM users WHERE uid="+
                                                    id + " LIMIT 1;");
                                            rs2.next();
                                            ResultSet rs3 = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM newfriend_" + rs.getInt("uid") +
                                                    " WHERE uid="+ id + ";");
                                            rs3.next();
                                            if(rs3.getInt("COUNT(*)") == 0){
                                                DBAccess.getDBA().execute("INSERT INTO newfriend_" + rs.getInt("uid") + "(uid,account) " +
                                                        "VALUES(" + id + ",'" + rs2.getString("account") + "');");
                                                resObj = new ResponseObject(ResponseObject.RES_ADDNEWFRIEND, new NewFriendInfoRes("success"));
                                                oos.writeObject(resObj);
                                            }else {
                                                resObj = new ResponseObject(ResponseObject.RES_ADDNEWFRIEND, new NewFriendInfoRes("duplicate"));
                                                oos.writeObject(resObj);
                                            }
                                        }else {
                                            resObj = new ResponseObject(ResponseObject.RES_ADDNEWFRIEND, new NewFriendInfoRes("fail"));
                                            oos.writeObject(resObj);
                                        }
                                        break;
                                    case RequestObject.REQ_CONFIRMADDNEWFRIEND:
                                        newFriendInfo = (NewFriendInfo)reqObj.getReqBody();
                                        id = findSocketID(ooslist,oos);
                                        DBAccess.getDBA().execute("DELETE FROM newfriend_"+id+" WHERE account='" + newFriendInfo.getUsername() + "';");
                                        rs = DBAccess.getDBA().executeQuery("SELECT uid FROM users WHERE BINARY account='" +
                                                newFriendInfo.getUsername() + "';");
                                        rs.next();
                                        DBAccess.getDBA().execute("INSERT INTO contact_"+id+"(uid,account) " +
                                                "VALUES(" + rs.getInt("uid") + ",'" + newFriendInfo.getUsername() + "');");
                                        ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT account FROM users WHERE uid=" +
                                                id + " LIMIT 1;");
                                        rs2.next();
                                        DBAccess.getDBA().execute("INSERT INTO contact_"+rs.getInt("uid")+"(uid,account) " +
                                                "VALUES(" + id + ",'" + rs2.getString("account") + "');");
                                        rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM newfriend_"+ id +
                                                " ;");
                                        rs.next();
                                        count = rs.getInt("COUNT(*)");
                                        int[] userlist = new  int[count];
                                        String[] username = new String[count];
                                        rs = DBAccess.getDBA().executeQuery("SELECT * FROM newfriend_"+ id + ";");
                                        int i = 0;
                                        while(rs.next()){
                                            userlist[i] = rs.getInt("uid");
                                            username[i++] = rs.getString("account");
                                        }
                                        resObj = new ResponseObject(ResponseObject.RES_LOADMESSAGEBOX,new LoadMessageboxInfoRes(userlist,username));
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_REFUSEADDNEWFRIEND:
                                        newFriendInfo = (NewFriendInfo)reqObj.getReqBody();
                                        id = findSocketID(ooslist,oos);
                                        DBAccess.getDBA().execute("DELETE FROM newfriend_"+id+" WHERE account='" + newFriendInfo.getUsername() + "';");
                                        rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM newfriend_"+ id +
                                                " ;");
                                        rs.next();
                                        count = rs.getInt("COUNT(*)");
                                        userlist = new  int[count];
                                        username = new String[count];
                                        rs = DBAccess.getDBA().executeQuery("SELECT * FROM newfriend_"+ id + ";");
                                        i = 0;
                                        while(rs.next()){
                                            userlist[i] = rs.getInt("uid");
                                            username[i++] = rs.getString("account");
                                        }
                                        resObj = new ResponseObject(ResponseObject.RES_LOADMESSAGEBOX,new LoadMessageboxInfoRes(userlist,username));
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_LOADMESSAGEBOX:
                                        id = findSocketID(ooslist,oos);
                                        rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM newfriend_"+ id +
                                                " ;");
                                        rs.next();
                                        count = rs.getInt("COUNT(*)");
                                        userlist = new  int[count];
                                        username = new String[count];
                                        rs = DBAccess.getDBA().executeQuery("SELECT * FROM newfriend_"+ id + ";");
                                        i = 0;
                                        while(rs.next()){
                                            userlist[i] = rs.getInt("uid");
                                            username[i++] = rs.getString("account");
                                        }
                                        resObj = new ResponseObject(ResponseObject.RES_LOADMESSAGEBOX,new LoadMessageboxInfoRes(userlist,username));
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_LOADFRIENDLIST:
                                        LoadfriendlistInfo loadfriendlistInfo = (LoadfriendlistInfo) reqObj.getReqBody();
                                        id = findSocketID(ooslist,oos);
                                        rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM contact_"+ id +
                                                " ;");
                                        rs.next();
                                        count = rs.getInt("COUNT(*)");
                                        userlist = new  int[count];
                                        username = new String[count];
                                        int[] useronline = new int[count];
                                        int[] userbattle = new int[count];
                                        rs = DBAccess.getDBA().executeQuery("SELECT users.uid,users.account,online,in_battle FROM users,contact_"+ id +
                                                " WHERE users.uid=contact_" + id +".uid;");
                                        i = 0;
                                        while(rs.next()){
                                            userlist[i] = rs.getInt("uid");
                                            username[i] = rs.getString("account");
                                            useronline[i] = rs.getInt("online");
                                            userbattle[i++] = rs.getInt("in_battle");
                                        }
                                        resObj = new ResponseObject(ResponseObject.RES_LOADFRIENDLIST,new LoadfriendlistInfoRes(userlist,username,useronline,userbattle));
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_LOADUSERINFO:
                                        LoadUserInfo loadUserInfo = (LoadUserInfo) reqObj.getReqBody();
                                        if(loadUserInfo.isIsself()){
                                            rs = DBAccess.getDBA().executeQuery("SELECT account,win,lose FROM users WHERE uid="
                                                    + findSocketID(ooslist,oos) + " LIMIT 1;");
                                        }else {
                                            rs = DBAccess.getDBA().executeQuery("SELECT account,win,lose FROM users WHERE BINARY account='"
                                                    + loadUserInfo.getUsername() + "' LIMIT 1;");
                                        }
                                        rs.next();
                                        resObj = new ResponseObject(ResponseObject.RES_LOADUSERINFO,new LoadUserInfoRes(rs.getString("account"),rs.getInt("win"),rs.getInt("lose")));
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_DELETEFRIEND:
                                        newFriendInfo = (NewFriendInfo)reqObj.getReqBody();
                                        id = findSocketID(ooslist,oos);
                                        DBAccess.getDBA().execute("DELETE FROM contact_"+id+" WHERE account='" + newFriendInfo.getUsername() + "';");
                                        rs = DBAccess.getDBA().executeQuery("SELECT uid FROM users WHERE BINARY account='" +
                                                newFriendInfo.getUsername() + "';");
                                        rs.next();
                                        DBAccess.getDBA().execute("DELETE FROM contact_"+rs.getInt("uid")+" WHERE uid=" + id + ";");

                                        rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM contact_"+ id +
                                                " ;");
                                        rs.next();
                                        count = rs.getInt("COUNT(*)");
                                        userlist = new  int[count];
                                        username = new String[count];
                                        useronline = new int[count];
                                        userbattle = new int[count];
                                        rs = DBAccess.getDBA().executeQuery("SELECT users.uid,users.account,online,in_battle FROM users,contact_"+ id +
                                                " WHERE users.uid=contact_" + id +".uid;");
                                        i = 0;
                                        while(rs.next()){
                                            userlist[i] = rs.getInt("uid");
                                            username[i] = rs.getString("account");
                                            useronline[i] = rs.getInt("online");
                                            userbattle[i++] = rs.getInt("in_battle");
                                        }
                                        resObj = new ResponseObject(ResponseObject.RES_LOADFRIENDLIST,new LoadfriendlistInfoRes(userlist,username,useronline,userbattle));
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_MODIFYACCOUNT:
                                        ModifyAccountInfo modifyAccountInfo = (ModifyAccountInfo) reqObj.getReqBody();
                                        rs = DBAccess.getDBA().executeQuery("SELECT uid FROM users WHERE BINARY password='" +
                                                modifyAccountInfo.getOldPassword() + "' AND BINARY account='" +
                                                modifyAccountInfo.getAccount() + "' LIMIT 1;");
                                        if(rs.next()){
                                            id = rs.getInt("uid");
                                            DBAccess.getDBA().execute("UPDATE users SET password='"+ modifyAccountInfo.getNewPassword()+"' WHERE uid="
                                                    +id+";");
                                            resObj = new ResponseObject(ResponseObject.RES_MODIFYACCOUNT, new ModifyAccountInfoRes("successfully modify account",true));
                                        }
                                        else{
                                            resObj = new ResponseObject(ResponseObject.RES_MODIFYACCOUNT, new ModifyAccountInfoRes("fail to modify account",false));
                                        }
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_MATCHGAME:
                                        MatchGameInfo matchGameInfo = (MatchGameInfo) reqObj.getReqBody();
                                        id = findSocketID(ooslist,oos);
                                        switch (matchGameInfo.getGameType()){
                                            case MatchGameInfo.GAME_READY:
                                                DBAccess.getDBA().execute("UPDATE users SET in_battle="+ 1+" WHERE uid="
                                                        +id+";");
                                                int id2 = MatchGameHandler.getMgHdler().pop();
                                                if(id2 >= 0){
                                                    MatchGameInfoRes matchGameInfoRes = new MatchGameInfoRes(MatchGameInfoRes.GAME_START);
                                                    ChessHandler chessHandler = new ChessHandler();
                                                    chessHandler.generateChessBoard();
                                                    matchGameInfoRes.setChessBoard(chessHandler.getMChessBoard());
                                                    matchGameInfoRes.setSelfStart(false);
                                                    matchGameInfoRes.setUid(id2);
                                                    rs = DBAccess.getDBA().executeQuery("SELECT account,win,lose FROM users WHERE uid=" + id2
                                                            +" LIMIT 1;");
                                                    rs.next();
                                                    DecimalFormat decimalFormat=new DecimalFormat(".00%");
                                                    matchGameInfoRes.setWinRate(decimalFormat.format(1.00f * rs.getInt("win")/(rs.getInt("win") + rs.getInt("lose"))));
                                                    matchGameInfoRes.setAccount(rs.getString("account"));
                                                    resObj = new ResponseObject(ResponseObject.RES_MATCHGAME,matchGameInfoRes);
                                                    oos.writeObject(resObj);
                                                    matchGameInfoRes.setUid(id);
                                                    matchGameInfoRes.setSelfStart(true);
                                                    rs = DBAccess.getDBA().executeQuery("SELECT account,win,lose FROM users WHERE uid=" + id
                                                            +" LIMIT 1;");
                                                    rs.next();
                                                    matchGameInfoRes.setWinRate(decimalFormat.format(1.00f * rs.getInt("win")/(rs.getInt("win") + rs.getInt("lose"))));
                                                    matchGameInfoRes.setAccount(rs.getString("account"));
                                                    resObj = new ResponseObject(ResponseObject.RES_MATCHGAME,matchGameInfoRes);
                                                    ooslist[id2].writeObject(resObj);
                                                }else {
                                                    MatchGameHandler.getMgHdler().push(id);
                                                }
                                                break;
                                            case MatchGameInfo.GAME_ON_TURN:
                                                MatchGameInfoRes matchGameInfoRes = new MatchGameInfoRes(MatchGameInfoRes.GAME_ON_TURN);
                                                matchGameInfoRes.setOldI(matchGameInfo.getOldI());
                                                matchGameInfoRes.setOldJ(matchGameInfo.getOldJ());
                                                resObj = new ResponseObject(ResponseObject.RES_MATCHGAME,matchGameInfoRes);
                                                ooslist[matchGameInfo.getUid()].writeObject(resObj);
                                                break;
                                            case MatchGameInfo.GAME_ON_MOVE:
                                                matchGameInfoRes = new MatchGameInfoRes(MatchGameInfoRes.GAME_ON_MOVE);
                                                matchGameInfoRes.setOldI(matchGameInfo.getOldI());
                                                matchGameInfoRes.setOldJ(matchGameInfo.getOldJ());
                                                matchGameInfoRes.setNewI(matchGameInfo.getNewI());
                                                matchGameInfoRes.setNewJ(matchGameInfo.getNewJ());
                                                resObj = new ResponseObject(ResponseObject.RES_MATCHGAME,matchGameInfoRes);
                                                ooslist[matchGameInfo.getUid()].writeObject(resObj);
                                                break;
                                            case MatchGameInfo.GAME_END:
                                                rs = DBAccess.getDBA().executeQuery("SELECT win,lose FROM users WHERE uid="+id+" LIMIT 1;");
                                                DBAccess.getDBA().execute("UPDATE users SET in_battle="+ 0 +" WHERE uid="
                                                        +id+";");
                                                rs.next();
                                                if(matchGameInfo.isSelfWin()){
                                                    DBAccess.getDBA().execute("UPDATE users SET win="+ (rs.getInt("win")+1) +" WHERE uid="
                                                            +id+";");
                                                }else {
                                                    DBAccess.getDBA().execute("UPDATE users SET lose="+ (rs.getInt("lose")+1) +" WHERE uid="
                                                            +id+";");
                                                }
                                                break;
                                            case MatchGameInfo.GAME_CACNCEL:
                                                DBAccess.getDBA().execute("UPDATE users SET in_battle="+ 0 +" WHERE uid="
                                                        +id+";");
                                                MatchGameHandler.getMgHdler().delete(id);
                                                break;
                                            default:
                                                break;
                                        }
                                        break;
                                    case RequestObject.REQ_FRIENDGAME:
                                        FriendGameInfo friendGameInfo = (FriendGameInfo) reqObj.getReqBody();
                                        id = findSocketID(ooslist,oos);
                                        switch (friendGameInfo.getGameType()){
                                            case FriendGameInfo.GAME_READY:
                                                DBAccess.getDBA().execute("UPDATE users SET in_battle="+ 1+" WHERE uid="
                                                        +id+";");
                                                DBAccess.getDBA().execute("UPDATE users SET in_battle="+ 1+" WHERE uid="
                                                        +friendGameInfo.getUid()+";");
                                                rs = DBAccess.getDBA().executeQuery("SELECT account FROM users WHERE uid="
                                                        +id+" LIMIT 1;");
                                                rs.next();
                                                FriendGameInfoRes friendGameInfoRes = new FriendGameInfoRes(FriendGameInfoRes.GAME_INVITE);
                                                friendGameInfoRes.setUid(id);
                                                friendGameInfoRes.setAccount(rs.getString("account"));
                                                resObj = new ResponseObject(ResponseObject.RES_FRIENDGAME,friendGameInfoRes);
                                                ooslist[friendGameInfo.getUid()].writeObject(resObj);
                                                break;
                                            case FriendGameInfo.GAME_CONFIRMREADY:
                                                friendGameInfoRes = new FriendGameInfoRes(FriendGameInfoRes.GAME_START);
                                                ChessHandler chessHandler = new ChessHandler();
                                                chessHandler.generateChessBoard();
                                                friendGameInfoRes.setChessBoard(chessHandler.getMChessBoard());
                                                friendGameInfoRes.setSelfStart(false);
                                                friendGameInfoRes.setUid(friendGameInfo.getUid());
                                                rs = DBAccess.getDBA().executeQuery("SELECT account,win,lose FROM users WHERE uid="
                                                        +friendGameInfo.getUid()+" LIMIT 1;");
                                                rs.next();
                                                DecimalFormat decimalFormat=new DecimalFormat(".00%");
                                                friendGameInfoRes.setWinRate(decimalFormat.format(1.00f * rs.getInt("win")/(rs.getInt("win") + rs.getInt("lose"))));
                                                friendGameInfoRes.setAccount(rs.getString("account"));
                                                resObj = new ResponseObject(ResponseObject.RES_FRIENDGAME,friendGameInfoRes);
                                                oos.writeObject(resObj);
                                                friendGameInfoRes.setUid(id);
                                                friendGameInfoRes.setSelfStart(true);
                                                rs = DBAccess.getDBA().executeQuery("SELECT account,win,lose FROM users WHERE uid="
                                                        +id+" LIMIT 1;");
                                                rs.next();
                                                friendGameInfoRes.setWinRate(decimalFormat.format(1.00f * rs.getInt("win")/(rs.getInt("win") + rs.getInt("lose"))));
                                                friendGameInfoRes.setAccount(rs.getString("account"));
                                                resObj = new ResponseObject(ResponseObject.RES_FRIENDGAME,friendGameInfoRes);
                                                ooslist[friendGameInfo.getUid()].writeObject(resObj);
                                                break;
                                            case FriendGameInfo.GAME_ON_TURN:
                                                friendGameInfoRes = new FriendGameInfoRes(FriendGameInfoRes.GAME_ON_TURN);
                                                friendGameInfoRes.setOldI(friendGameInfo.getOldI());
                                                friendGameInfoRes.setOldJ(friendGameInfo.getOldJ());
                                                resObj = new ResponseObject(ResponseObject.RES_FRIENDGAME,friendGameInfoRes);
                                                ooslist[friendGameInfo.getUid()].writeObject(resObj);
                                                break;
                                            case FriendGameInfo.GAME_ON_MOVE:
                                                friendGameInfoRes = new FriendGameInfoRes(FriendGameInfoRes.GAME_ON_MOVE);
                                                friendGameInfoRes.setOldI(friendGameInfo.getOldI());
                                                friendGameInfoRes.setOldJ(friendGameInfo.getOldJ());
                                                friendGameInfoRes.setNewI(friendGameInfo.getNewI());
                                                friendGameInfoRes.setNewJ(friendGameInfo.getNewJ());
                                                resObj = new ResponseObject(ResponseObject.RES_FRIENDGAME,friendGameInfoRes);
                                                ooslist[friendGameInfo.getUid()].writeObject(resObj);
                                                break;
                                            case FriendGameInfo.GAME_END:
                                                rs = DBAccess.getDBA().executeQuery("SELECT win,lose FROM users WHERE uid="+id+" LIMIT 1;");
                                                DBAccess.getDBA().execute("UPDATE users SET in_battle="+ 0 +" WHERE uid="
                                                        +id+";");
                                                rs.next();
                                                if(friendGameInfo.isSelfWin()){
                                                    DBAccess.getDBA().execute("UPDATE users SET win="+ (rs.getInt("win")+1) +" WHERE uid="
                                                            +id+";");
                                                }else {
                                                    DBAccess.getDBA().execute("UPDATE users SET lose="+ (rs.getInt("lose")+1) +" WHERE uid="
                                                            +id+";");
                                                }
                                                break;
                                            case FriendGameInfo.GAME_CACNCEL:
                                                DBAccess.getDBA().execute("UPDATE users SET in_battle="+ 0 +" WHERE uid="
                                                        +id+";");
                                                DBAccess.getDBA().execute("UPDATE users SET in_battle="+ 0 +" WHERE uid="
                                                        +friendGameInfo.getUid()+";");
                                                friendGameInfoRes = new FriendGameInfoRes(FriendGameInfoRes.GAME_CANCEL);
                                                friendGameInfoRes.setUid(id);
                                                resObj = new ResponseObject(ResponseObject.RES_FRIENDGAME,friendGameInfoRes);
                                                ooslist[friendGameInfo.getUid()].writeObject(resObj);
                                                break;
                                            default:
                                                break;
                                        }
                                        break;
                                    case RequestObject.REQ_OPERATIONHELP:
                                        String help = "     双方轮流翻棋，任意一方累计翻出两个相同颜色的棋子，便决定该方阵营归属为该颜色。" +
                                                "阵营确定后，双方可以移动手中棋子、吃掉对方棋子、翻开棋子操作，" +
                                                "当敌方三颗地雷被挖走后，敌方军旗可以被任意我方可移动兵种抢夺，" +
                                                "率先夺得对方军旗者为胜方。";
                                        resObj = new ResponseObject(ResponseObject.RES_OPERATIONHELP,new HelpInfoRes(help));
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_APPLICATIONHELP:
                                        help = "     随机匹配：用户A点击随机匹配按钮等待匹配，用户B也点击随机匹配按钮，与A完成匹配，进行对战。\n\n" +
                                                "     好友对战：用户A点击好友对战里的好友对战场来等待好友与之PK。" +
                                                "好友B点击好友对战里的好友列表中用户A的最右边“双剑”图标进行与A的好友对战。\n\n" +
                                                "     添加好友：用户A点击好友对战里的添加好友按钮，输入待添加用户B的用户名。" +
                                                "用户B将会在消息盒子中接受到添加请求，B可以选择接受好友添加或者拒绝。";
                                        resObj = new ResponseObject(ResponseObject.RES_APPLICATIONHELP,new HelpInfoRes(help));
                                        oos.writeObject(resObj);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            try {
                                clientsocket.close();
                            } catch (IOException ee) {
                                ee.printStackTrace();
                            }
                            if (id >= 0) {
                                ooslist[id] = null;
                                DBAccess.getDBA().execute("UPDATE users SET online=0 WHERE uid="
                                        + id + ";");
                                DBAccess.getDBA().execute("UPDATE users SET in_battle=0 WHERE uid="
                                        + id + ";");
                            }
                        }
                    }
                }).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int findSocketID(ObjectOutputStream[] ooslist, ObjectOutputStream oos) {
        for (int i = 0; i < ooslist.length; i++)
            if (ooslist[i] == oos)
                return i;
        return -1;
    }
}
