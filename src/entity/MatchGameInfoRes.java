package entity;

import java.io.Serializable;

/**
 * Created by Administrator1 on 2017/4/21.
 */
public class MatchGameInfoRes implements Serializable {
    public static final int GAME_START = 1;
    public static final int GAME_ON_TURN = 2;
    public static final int GAME_ON_MOVE = 3;
    private int uid;
    private Chess[][] chessBoard;
    private boolean selfStart;
    private int oldI,oldJ,newI,newJ;
    private int gameType;
    private String account;
    private String winRate;
    public MatchGameInfoRes(int type){
        gameType = type;
    }
    @Override
    public String toString(){
        return "gameType: "+ gameType;
    }
    public int getGameType(){
        return gameType;
    }
    public int getUid(){
        return uid;
    }
    public void setUid(int id){
        uid = id;
    }
    public Chess[][] getChessBoard(){
        return chessBoard;
    }
    public void setChessBoard(Chess[][] chessboard){
        chessBoard = chessboard;
    }

    public boolean isSelfStart(){
        return selfStart;
    }
    public void setSelfStart(boolean win){
        selfStart = win;
    }

    public int getOldI(){
        return oldI;
    }
    public void setOldI(int i){
        oldI = i;
    }

    public int getOldJ(){
        return oldJ;
    }
    public void setOldJ(int j){
        oldJ = j;
    }

    public int getNewI(){
        return newI;
    }
    public void setNewI(int i){
        newI = i;
    }

    public int getNewJ(){
        return newJ;
    }
    public void setNewJ(int j){
        newJ = j;
    }

    public String getAccount(){
        return  account;
    }
    public void  setAccount(String name){
        account = name;
    }

    public String getWinRate(){
        return winRate;
    }
    public void setWinRate(String rate){
        winRate = rate;
    }

}
