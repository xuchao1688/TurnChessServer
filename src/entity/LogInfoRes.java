package entity;
import java.io.Serializable;

public class LogInfoRes implements Serializable{
    private String message;
    private boolean success;
    public LogInfoRes(String mess, boolean succ){
        message = mess;
        success = succ;
    }
    @Override
    public String toString(){
        return "message is '" + message +"',"+ "result is '"+ success +"'.";
    }
    public String getMessage(){
        return message;
    }
    public boolean isSuccess(){
        return success;
    }
}
