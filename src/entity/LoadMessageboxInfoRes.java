package entity;

import java.io.Serializable;

/**
 * Created by Administrator1 on 2017/4/20.
 */
public class LoadMessageboxInfoRes implements Serializable {
    private int[] userlist;
    private String[] username;
    public LoadMessageboxInfoRes(int[] list,String[] name){
        userlist = list;
        username = name;
    }
    @Override
    public String toString(){
        return "user number is " + userlist.length;
    }
    public int[] getUserlist(){
        return userlist;
    }
    public String[] getUsername(){
        return username;
    }
}
