package entity;

import java.io.Serializable;

/**
 * Created by Administrator1 on 2017/4/21.
 */
public class ModifyAccountInfo implements Serializable{
    private String account;
    private String oldPassword;
    private String newPassword;
    public ModifyAccountInfo(String name, String oPass, String nPass){
        account = name;
        oldPassword = oPass;
        newPassword = nPass;
    }
    @Override
    public String toString(){
        return "account is " + account + ", old password is " + oldPassword + ", new password is " + newPassword;
    }
    public String getAccount(){
        return account;
    }
    public String getOldPassword(){
        return oldPassword;
    }
    public String getNewPassword(){
        return newPassword;
    }
}
