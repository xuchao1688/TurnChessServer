package entity;
import java.io.Serializable;

public class LoadfriendlistInfoRes implements Serializable{
    private int[] userlist;
    private String[] username;
    private int[] useronline;
    private int[] userbattle;
    public LoadfriendlistInfoRes(int[] list,String[] name, int[] online,int[] battle ){
        userlist = list;
        username = name;
        useronline = online;
        userbattle = battle;
    }
    @Override
    public String toString(){
        return "user number is " + userlist.length;
    }
    public int[] getUserlist(){
        return userlist;
    }
    public int[] getUseronline(){
        return useronline;
    }
    public int[] getUserbattle(){
        return userbattle;
    }
    public String[] getUsername(){
        return username;
    }
}
