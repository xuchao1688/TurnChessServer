package entity;
import java.io.Serializable;

/**
 * Created by Administrator1 on 2017/3/26.
 */

public class RequestObject implements Serializable {
    public static final int REQ_REG = 1;
    public static final int REQ_LOG = 2;
    public static final int REQ_LOADFRIENDLIST = 3;
    public static final int REQ_ADDNEWFRIEND = 4;
    public static final int REQ_CONFIRMADDNEWFRIEND = 5;
    public static final int REQ_SENDMESSAGE = 6;
    public static final int REQ_LOADHISTORYCHAT = 7;
    public static final int REQ_CREATENEWGROUP = 8;
    public static final int REQ_LOADGROUPLIST = 9;
    public static final int REQ_LOADGROUPHISTORYCHAT = 10;
    public static final int REQ_SENDGROUPMESSAGE = 11;
    public static final int REQ_LOADMESSAGEBOX = 12;
    public static final int REQ_REFUSEADDNEWFRIEND = 13;
    public static final int REQ_LOADUSERINFO = 14;
    public static final int REQ_DELETEFRIEND = 15;
    public static final int REQ_MODIFYACCOUNT = 16;
    public static final int REQ_MATCHGAME = 17;
    public static final int REQ_FRIENDGAME = 18;
    public static final int REQ_OPERATIONHELP = 19;
    public static final int REQ_APPLICATIONHELP = 20;
    private int reqType;
    private Object reqBody;
    public RequestObject(int reqType,Object reqBody){
        super();
        this.reqType = reqType;
        this.reqBody = reqBody;
    }
    public int getReqType(){
        return reqType;
    }
    public Object getReqBody(){
        return reqBody;
    }
    @Override
    public String toString(){
        return "Request Type: " + reqType + " .Request Content: " + reqBody;
    }
}
