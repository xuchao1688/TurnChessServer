package entity;

import java.io.Serializable;

/**
 * Created by Administrator1 on 2017/4/21.
 */
public class LoadUserInfo implements Serializable {
    private String account;
    private boolean isself;

    public LoadUserInfo(String name, boolean self){
        account = name;
        isself = self;
    }
    @Override
    public String toString(){
        return "account is " + account + ". Is self: " + isself;
    }
    public String getUsername(){
        return account;
    }
    public boolean isIsself(){
        return isself;
    }
}
