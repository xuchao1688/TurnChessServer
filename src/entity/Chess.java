package entity;

import java.io.Serializable;

/**
 * Created by Administrator1 on 2017/4/21.
 */

public class Chess implements Serializable{
    public static final int CHESS_NULL = 0;        //无
    public static final int CHESS_COMMANDER = 1;   //司令
    public static final int CHESS_ARMY = 2;      //军长
    public static final int CHESS_TEACHER = 3;    //师长
    public static final int CHESS_BRIGADIER = 4;   //旅长
    public static final int CHESS_COLONEL = 5;     //团长
    public static final int CHESS_BATTALION = 6;    //营长
    public static final int CHESS_COMPANY = 7;     //连长
    public static final int CHESS_PLATOON= 8;      //排长
    public static final int CHESS_SAPPER = 9;      //工兵
    public static final int CHESS_MINE = 10;       //地雷
    public static final int CHESS_BOMB = 11;       //炸弹
    public static final int CHESS_FLAG = 12;       //军棋
    public static final int STATE_NOTDISCOVERED = 0;  //未被翻面
    public static final int STATE_ALIVE = 1;      //活动
    public static final int STATE_DEAD = 2;        //死亡
    public static final int STATE_SELECT = 3;      //被选中
    private int armyClass;       //军种
    private int state;            //状态
    private boolean isEnemy;
    public Chess(int army, int stat,boolean enemy){
        armyClass = army;
        state = stat;
        isEnemy = enemy;
    }
    public int getState(){
        return state;
    }
    public int getArmyClass(){
        return armyClass;
    }
    public boolean isEnemy(){
        return isEnemy;
    }
    public void setState(int stat){
        state = stat;
    }
}
