/**
 * Created by Administrator1 on 2017/4/21.
 */
import entity.Chess;

import java.util.Date;
import java.util.Random;

public class ChessHandler {
    private Chess[][] mChessBoard;
    public void generateChessBoard(){
        Chess[][] chessboard = new Chess[12][5];
                for(int i = 0;i<12;i++){ //******debug
            for(int j = 0;j<5;j++){
                chessboard[i][j] = new Chess(Chess.CHESS_NULL,Chess.STATE_ALIVE,false);
            }
        }
        for(int k = 0;k <= 1;k++){
            chessboard[11*k][4*k] = new Chess(Chess.CHESS_FLAG,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k][4*k+(1-2*k)] = new Chess(Chess.CHESS_COMMANDER,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k][4*k+(1-2*k)*2] = new Chess(Chess.CHESS_ARMY,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k][4*k+(1-2*k)*3] = new Chess(Chess.CHESS_TEACHER,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k][4*k+(1-2*k)*4] = new Chess(Chess.CHESS_TEACHER,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)][4*k] = new Chess(Chess.CHESS_BRIGADIER,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)][4*k+(1-2*k)] = new Chess(Chess.CHESS_BRIGADIER,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)][4*k+(1-2*k)*2] = new Chess(Chess.CHESS_COLONEL,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)][4*k+(1-2*k)*3] = new Chess(Chess.CHESS_COLONEL,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)][4*k+(1-2*k)*4] = new Chess(Chess.CHESS_BATTALION,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*2][4*k] = new Chess(Chess.CHESS_BATTALION,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*2][4*k+(1-2*k)] = new Chess(Chess.CHESS_NULL,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*2][4*k+(1-2*k)*2] = new Chess(Chess.CHESS_COMPANY,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*2][4*k+(1-2*k)*3] = new Chess(Chess.CHESS_NULL,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*2][4*k+(1-2*k)*4] = new Chess(Chess.CHESS_COMPANY,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*3][4*k] = new Chess(Chess.CHESS_COMPANY,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*3][4*k+(1-2*k)] = new Chess(Chess.CHESS_PLATOON,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*3][4*k+(1-2*k)*2] = new Chess(Chess.CHESS_NULL,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*3][4*k+(1-2*k)*3] = new Chess(Chess.CHESS_PLATOON,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*3][4*k+(1-2*k)*4] = new Chess(Chess.CHESS_PLATOON,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*4][4*k] = new Chess(Chess.CHESS_SAPPER,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*4][4*k+(1-2*k)] = new Chess(Chess.CHESS_NULL,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*4][4*k+(1-2*k)*2] = new Chess(Chess.CHESS_SAPPER,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*4][4*k+(1-2*k)*3] = new Chess(Chess.CHESS_NULL,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*4][4*k+(1-2*k)*4] = new Chess(Chess.CHESS_SAPPER,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*5][4*k] = new Chess(Chess.CHESS_MINE,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*5][4*k+(1-2*k)] = new Chess(Chess.CHESS_MINE,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*5][4*k+(1-2*k)*2] = new Chess(Chess.CHESS_MINE,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*5][4*k+(1-2*k)*3] = new Chess(Chess.CHESS_BOMB,Chess.STATE_NOTDISCOVERED,k==0);
            chessboard[11*k+(1-2*k)*5][4*k+(1-2*k)*4] = new Chess(Chess.CHESS_BOMB,Chess.STATE_NOTDISCOVERED,k==0);
        }
        Random random = new Random(new Date().getTime());
        for(int i=0;i<60;i++){
            int randomIndex = random.nextInt(60);
            if(i == 11 || i == 13||i == 17||i == 21||i == 23||i == 46||i == 36||i == 42||i == 38||i == 48){
                continue;
            }
            while(randomIndex == 11 || randomIndex == 13||randomIndex == 17||randomIndex == 21||randomIndex == 23||randomIndex == 46||randomIndex == 36||randomIndex == 42||randomIndex == 38||randomIndex == 48 ){
                randomIndex = random.nextInt(60);
            }
            Chess tmp = chessboard[i/5][i%5];
            chessboard[i/5][i%5] = chessboard[randomIndex/5][randomIndex%5];
            chessboard[randomIndex/5][randomIndex%5] = tmp;
        }
        mChessBoard = chessboard;
    }
    public Chess[][] getMChessBoard(){
        return mChessBoard;
    }
}
